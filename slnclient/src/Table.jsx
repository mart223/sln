import React from "react"
import {TableItem} from "./TableItem"
import {Query} from "react-apollo"
import gql from "graphql-tag"


const FEED_QUERY = gql`
  query {
  newsList(skip: 0, limit: 20) {
    rows {
      id
      title
      content
      url
      img
      comments {
        id
        email
        content
        createdDate
      }
    }
    totalRows
  }
}
`

export function Table(props) {
    
    
    return (
        
        [
            <div className={"TableTitle"} key={"TableTitle"}>
                
                <h2> NEWS READER </h2>
            
            </div>,
            <div className={"Table"}>
            <Query query={FEED_QUERY} key={"FeedQuery"}>
                {({loading, error, data}) => {
                    
                    if (loading) {
                        return <p>Loading...</p>
                    }
                    if (error) {
                        return <p>Error</p>
                    }
                    
                    console.log(data)
                    
                    return data.newsList.rows.map((item, index) => <TableItem item={item} index={index} key={item.id}/>)
                    
                }}
            </Query>
            </div>
        ]
    
    )
    
}










