import React, {useCallback} from "react"
import gql from "graphql-tag"
import {useQuery} from "react-apollo"
import {useHistory} from "react-router-dom"


const ITEM_QUERY = gql`
  query($id: ID!) {
  newsItem(id: $id) {
    id
    title
    content
    url
    img
    comments {
      id
      email
      content
      createdDate
    }
  }
}
`

export function Article(props) {
    
    const history = useHistory()
    const closeItem = useCallback((event) => {
        
        history.push("/")
        
    }, [history])
    
    const itemID = props.match.params.itemID
    const {loading, error, data} = useQuery(ITEM_QUERY, {variables: {id: itemID}})
    
    if (loading) {
        return <p>Loading...</p>
    }
    
    if (error) {
        return <p>Error</p>
    }
    
    let item = data.newsItem
    
    
    
    return (
        
        [
            <div className={"TableTitle"} key={"ArticleTitle"}>
                
                <div className={"BackToMain"} onClick={closeItem}> <h2> &#8592; </h2> </div>
                
                <h2> NEWS READER </h2>
            
            </div>,
            <div className={"ArticleView"} key={"ArticleContainer"}>
                
                <h3> {item.title} </h3>
                <img src={item.img || "logo192.png"}
                     alt={"Article"}/>
                
                <p> {item.content} </p>
                
                <a href={item.url}> Read more </a>
                
                {/*{JSON.stringify(item)}*/}
            
            </div>
        ]
    
    )
    
}










